import {
  AppRegistry
} from 'react-native';
import App from './src';

AppRegistry.registerComponent('w4m', () => App);
