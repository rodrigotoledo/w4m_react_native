/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { View, Text, TouchableHighlight } from "react-native";
import SignatureCapture from 'react-native-signature-capture';

import Icon from "react-native-vector-icons/FontAwesome";

import { colors } from "styles";
import styles from "./styles";

export default class SignatureJob extends Component {
  static navigationOptions = {
    title: "Firma de OS",
    headerTitleStyle: {
      textAlign: "center",
      alignSelf: "center",
      flex: 1,
      color: colors.primary
    },
    tabBarIcon: () => (
      <Icon name="hourglass-half" size={20} color={colors.lighter} />
    )
  };

  state = {
    loading: false
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
                <Text style={{alignItems:"center",justifyContent:"center"}}>Captura de firma extendida</Text>
                <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref="sign"
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>

                <View style={{ flex: 1, flexDirection: "row" }}>
                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.saveSign() } } >
                        <Text>Guardar</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.resetSign() } } >
                        <Text>Borrar</Text>
                    </TouchableHighlight>
                </View>
            </View>
    );
  }
}
