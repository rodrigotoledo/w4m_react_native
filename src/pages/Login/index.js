/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from "react";
import { NavigationActions } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import api from 'services/api';


import { View, Text, TextInput, TouchableOpacity, AsyncStorage, StatusBar, Image, ActivityIndicator } from "react-native";

import styles from "./styles";

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  static propTypes = {
    navigation: PropTypes.shape({
      dispatch: PropTypes.func,
    }).isRequired,
  };

  state = {
    // email: 'operador_w4m@w4m.com.br',
    email: 'rodrigo.toledo@proxer.com.br',
    // password: 'asdqwe123',
    password: 'asdqwe123',
  };

  checkUserExists = async (email, password) => {
    const data = {
      email: email,
      password: password,
      device_uuid: DeviceInfo.getUniqueID(),
      loading: false,
      errorMessage: null,
    };
    const user = await api.post('/users/sign_in', data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    });
    await AsyncStorage.setItem('@ClientKey:auth_token', user.data.auth_token);
    // await AsyncStorage.setItem('@ClientKey:jobs', JSON.stringify(user.data.jobs));
    return user;
  };

  saveUser = async (email) => {
    await AsyncStorage.setItem('@ClientKey:email', email);
  };

  signIn = async () => {
    const { email, password } = this.state;

    if (email === undefined || email.length === 0 || password === undefined || password.length === 0) return;

    this.setState({ loading: true });

    try{
      await this.checkUserExists(email, password);
      await this.saveUser(email);

      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'HomeJobs'}),
        ],
      })
      this.props.navigation.dispatch(resetAction);
    }catch(err){
      this.setState({ loading: false, errorMessage: 'Erro na autenticação, verifique os dados abaixo' });
    }
  };

  render() {
    return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />

      <Image source={require('images/logoLogin.png')} style={styles.logo} />
      <Text style={styles.description}>Preencha os campos abaixo para entrar no sistema</Text>
      { !!this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
      <View style={styles.form}>
        <TextInput autoCapitalize="none" autoCorrect={false} style={styles.input} placeholder="Digite seu email" underlineColorAndroid="rgba(0,0,0,0)" value={this.state.email} onChangeText={email => this.setState({email})} />
        <TextInput autoCapitalize="none" autoCorrect={false} style={[styles.input, styles.marginTop]} placeholder="Digite sua senha" value={this.state.password} underlineColorAndroid="rgba(0,0,0,0)" secureTextEntry={true} onChangeText={password => this.setState({password})} />

        <TouchableOpacity style={styles.button} onPress={this.signIn}>
          {
            this.state.loading ? <ActivityIndicator size='small' color='#fff' /> : <Text style={styles.buttonText}><Icon name='md-log-in' color='#fff' size={16} /> Entrar</Text>
          }
        </TouchableOpacity>
      </View>
    </View>)
    ;
  }
};
