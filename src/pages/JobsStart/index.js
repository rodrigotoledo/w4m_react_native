/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";

import JobStart from "components/JobStart";

import Icon from "react-native-vector-icons/FontAwesome";

import { colors } from "styles";
import styles from "./styles";

export default class JobsCheckin extends Component {
  static navigationOptions = {
    title: "Datos del Servicio",
    headerTitleStyle: {
      textAlign: "center",
      alignSelf: "center",
      flex: 1,
      color: colors.primary
    },
    tabBarIcon: () => (
      <Icon name="hourglass-half" size={20} color={colors.lighter} />
    )
  };

  state = {
    loading: false
  };

  componentWillMount = () => {
    // this.getJobs();
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <ActivityIndicator style={styles.loading} />
        ) : (
          <JobStart />
        )}
      </View>
    );
  }
}
