import React, { Component } from 'react';
import { View } from "react-native";
import { NavigationActions } from "react-navigation";
import PropTypes from 'prop-types';

import { TouchableOpacity, AsyncStorage } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './styles';

export default class HeaderRight extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      dispatch: PropTypes.func,
    }).isRequired,
  }


  goToHome = async () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "HomeJobs" })]
    });
    this.props.navigation.dispatch(resetAction);
  };

  signOut = async () => {
    await AsyncStorage.clear();

    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Login" })]
    });
    this.props.navigation.dispatch(resetAction);
  };

  render() {
    return <View style={{ flexDirection: "row" }}>
        <TouchableOpacity onPress={this.goToHome} style={{ marginRight: 5 }}>
          <Icon name="retweet" size={25} style={styles.iconColors} />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.signOut}>
          <Icon name="sign-out" size={25} style={styles.iconColors} />
        </TouchableOpacity>
      </View>;
  }
}
