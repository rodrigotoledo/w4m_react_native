import { StyleSheet } from 'react-native';
import { colors } from 'styles';

const styles = StyleSheet.create({
  iconColors: {
    color: colors.primary,
  }
});

export default styles;
