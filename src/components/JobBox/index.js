import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, AsyncStorage } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';

import api from 'services/api';

import styles from './styles';

class JobBox extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      dispatch: PropTypes.func,
    }).isRequired,
  };


  state = {
    loading: false,
    latitude: null,
    longitude: null,
  };

  componentWillMount = () => {
    navigator.geolocation.watchPosition((position) => {
      this.setState({latitude: position.coords.latitude.toString()});
      this.setState({longitude: position.coords.longitude.toString()});
    });
  }

  buttonJobText = (job) => {
    switch (job.status.name) {
      case 'OPERADOR_ATRIBUIDO':
        return 'Iniciar Deslocamento';
      case 'INICIAR_DESLOCAMENTO':
        return 'Chegada ao Ponto';
      case 'CHEGADA':
        return 'Aguardando Execução';
      default:
        return 'Realizar serviço ou não';
    }
  };

  buttonJobIcon = (job) => {
    switch (job.status.name) {
      case 'OPERADOR_ATRIBUIDO':
        return 'md-send';
      case 'INICIAR_DESLOCAMENTO':
        return 'md-home';
      case 'CHEGADA':
        return 'md-clock';
      default:
        return 'md-hammer';
    }
  };

  updateJobStatus = async (job) => {
    this.setState({ loading: true });
    const authToken = await AsyncStorage.getItem('@ClientKey:auth_token');
    switch (job.status.name) {
      case 'OPERADOR_ATRIBUIDO':
      case 'INICIAR_DESLOCAMENTO':
      case 'CHEGADA':
        await this.goNextStatus(job, authToken);
        this.setState({ loading: false });
        this.props.navigation.navigate('HomeJobs', { job });
        break;
      default:
        await this.checkinJob(job, authToken);
        this.setState({ loading: false });
        this.props.navigation.navigate('DetailJob', { job });
    }
  };

  goNextStatus = async (job, authToken) => {
    await api.post('/jobs/update_status', {
      auth_token: authToken,
      id: job.id,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      loading: false,
      errorMessage: null,
    });
  };

  checkinJob = async (job, authToken) => {
    await api.post('/jobs/checkin', {
      auth_token: authToken,
      id: job.id,
      loading: false,
      errorMessage: null,
    });
  };

  render() {
    return (
      <View style={styles.jobContainer}>
        <View style={styles.row}>
          <View style={styles.rowSpecialAttention}>
            <Text style={styles.title}>OS #{this.props.job.id}</Text>
            <Text style={styles.title}>Situação: {this.props.job.status.description}</Text>
            <Text style={styles.subTitle}>Descrição: {this.props.job.description}</Text>
          </View>
          <View>
            <View style={styles.addressContainer}>
              <Text style={[styles.address, styles.subTitle, styles.underline]}>Ponto: {this.props.job.customer.name}</Text>
              <Text style={[styles.address, styles.subTitle]}>
                {this.props.job.customer.address_line},{' '}
                {this.props.job.customer.address_neighborhood} -{' '}
                {this.props.job.customer.address_city} /{' '}
                {this.props.job.customer.address_state}
              </Text>
            </View>
            <TouchableOpacity style={styles.button} onPress={() => this.updateJobStatus(this.props.job)}>
              {this.state.loading ? (
                <ActivityIndicator size='small' color='#fff' />
              ) : (
                <Text style={styles.buttonText}><Icon name={this.buttonJobIcon(this.props.job)} color='#fff' size={20} /> {this.buttonJobText(this.props.job)}</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
};

export default withNavigation(JobBox);
